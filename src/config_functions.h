#ifndef _CONFIG_FUNCTIONS_H_
#define _CONFIG_FUNCTIONS_H_

#include "torque_encoder.h"

void init_adc (void);
void init_thresholds (_prog_addressT EE_address, TE_THRESHOLDS *thresholds, uint16_t EE_thresholds[16]);


#endif