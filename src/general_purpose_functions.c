#include <xc.h>

#include "general_purpose_functions.h"

#include "send_functions.h"
#include "check_functions.h"
#include "update_functions.h"


void acquire_values (volatile bool *flag, TE_VALUES *values){

    if(*flag){

        toggle_interrupts(OFF);

        values->new_APPS_0 = ADCBUF0;
        values->new_APPS_1 = ADCBUF1;

        values->new_BPS_pressure_0 = ADCBUF4;
        values->new_BPS_pressure_1 = ADCBUF2;
    
        values->new_BPS_electric_0 = ADCBUF3;

        update_values(values);

        *flag = false;

        toggle_interrupts(ON);
    }
    return;   
}

void proccess_values (volatile bool *flag, 
                      TE_VALUES *values, 
                      TE_CORE *status,
                      TE_THRESHOLDS *thresholds,
                      COUNTERS *time_counters){

    if(*flag){

        toggle_interrupts(OFF);

        update_raw_averages (values);
 
        //check_sensors (values, status);
        
        update_treated_averages (values, thresholds, status);
        
        check_master (values, status, time_counters);

        send_messages (values, status);

        send_BPS_pressure_0_analog (status);

        *flag = false;

        toggle_interrupts(ON);
    }
    return;
}

void toggle_interrupts (INTERRUPT_TOGGLE toggle){
    
    toggle_adc_int(toggle);
    toggle_timer1_int(toggle);

    return;
}

void toggle_adc_int (INTERRUPT_TOGGLE toggle){

    if(toggle)
        IEC0bits.ADIE = 1;
    else    
        IEC0bits.ADIE = 0;
    
    return;
}

void toggle_timer1_int (INTERRUPT_TOGGLE toggle){

    if(toggle)
        IEC0bits.T1IE = 1;
    else    
        IEC0bits.T1IE = 0;
    
    return;
}

