#ifndef _SEND_FUNCTIONS_H
#define _SEND_FUNCTIONS_H

#include "torque_encoder.h"

#include "can-ids/Devices/TE_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"

#include "lib_pic30f/can.h"


void    send_BPS_eletric    (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message);
void    send_BPS_pressure   (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message);
void    send_APPS           (TE_VALUES *value, TE_CORE *status, TE_MESSAGE *main_message);
void    send_status         (TE_CORE *status, TE_MESSAGE *main_message);
void    send_debbug_message (TE_VALUES *value, TE_DEBUG_MESSAGE *debug_message);
void    send_messages       (TE_VALUES *value, TE_CORE *status);
void    send_init_msg       (void);
void    process_can_message (CANdata *r_message, 
                             TE_CORE *status, 
                             TE_VALUES *values, 
                             _prog_addressT EE_address,
                             uint16_t EE_thresholds[16],
                             TE_THRESHOLDS *thresholds);

void    send_BPS_pressure_0_analog  (TE_CORE *status);

#endif