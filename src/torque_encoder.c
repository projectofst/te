// DSPIC30F6012A Configuration Bit Settings

// 'C' source line config statements

// FOSC
#pragma config FOSFPR = HS2_PLL8		// Oscillator (HS2 w/PLL 8x)
#pragma config FCKSMEN = CSW_FSCM_OFF	// Clock Switching and Monitor (Sw Disabled, Mon Disabled)

// FWDT
#pragma config FWPSB = WDTPSB_16		// WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512		// WDT Prescaler A (1:512)
#pragma config WDT = WDT_OFF			// Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_OFF			// POR Timer Value (Timer Disabled)
#pragma config BODENV = NONE			// Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_OFF			// PBOR Enable (Disabled)
#pragma config MCLRE = MCLR_EN			// Master Clear Enable (Enabled)

// FBS
#pragma config BWRP = WR_PROTECT_BOOT_OFF// Boot Segment Program Memory Write Protect (Boot Segment Program Memory may be written)
#pragma config BSS = NO_BOOT_CODE		// Boot Segment Program Flash Memory Code Protection (No Boot Segment)
#pragma config EBS = NO_BOOT_EEPROM		// Boot Segment Data EEPROM Protection (No Boot EEPROM)
#pragma config RBS = NO_BOOT_RAM		// Boot Segment Data RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WR_PROT_SEC_OFF	// Secure Segment Program Write Protect (Disabled)
#pragma config SSS = NO_SEC_CODE		// Secure Segment Program Flash Memory Code Protection (No Secure Segment)
#pragma config ESS = NO_SEC_EEPROM		// Secure Segment Data EEPROM Protection (No Segment Data EEPROM)
#pragma config RSS = NO_SEC_RAM			// Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = GWRP_OFF			// General Code Segment Write Protect (Disabled)
#pragma config GCP = GSS_OFF			// General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD			// Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#include "torque_encoder.h"
#include "check_functions.h"
#include "config_functions.h"
#include "send_functions.h"
#include "update_functions.h"
#include "general_purpose_functions.h"

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/TE_CAN.h"

#include "lib_pic30f/ADC.h"
#include "lib_pic30f/timer.h"
#include "lib_pic30f/can.h"
#include "lib_pic30f/trap.h"


volatile bool Debug = true;

volatile bool adc_flag = false;
volatile bool timer1_flag = false;

TE_CORE status;
TE_VALUES value;
COUNTERS timer_counter;



TE_THRESHOLDS threshold;


_prog_addressT thresholds_EE_address;

uint16_t stored_EE_thresholds [16] = {0};
uint16_t _EEDATA(32) initial_EE_thresholds[16] = {

    APPS_0_LOWER_MECHANICAL_THRS,
    APPS_0_UPPER_MECHANICAL_THRS,
    APPS_1_LOWER_MECHANICAL_THRS,
    APPS_1_UPPER_MECHANICAL_THRS,
    BPS_ELECTRIC_LOWER_MECHANICAL_THRS,
    BPS_ELECTRIC_UPPER_MECHANICAL_THRS,
    BPS_PRESSURE_0_LOWER_MECHANICAL_THRS,
    BPS_PRESSURE_0_UPPER_MECHANICAL_THRS,
    BPS_PRESSURE_1_LOWER_MECHANICAL_THRS,
    BPS_PRESSURE_1_UPPER_MECHANICAL_THRS,
    0, 
    0, 
    0, 
    0, 
    0, 
    0
};



void adc_callback (void){
    
    adc_flag = true;
    return;
}


void timer1_callback (void)
{
    timer1_flag = true;
/*
    CANdata goma;
    goma.dev_id = DEVICE_ID_TE;
    goma.msg_id = 10;
    goma.dlc = 4;
    goma.data[0] = value.avg_BPS_pressure_0;
    goma.data[1] = value.avg_BPS_pressure_1;
    //goma.data[2] = stored_EE_thresholds[BPS_PRESSURE_1_LOWER_MEC];
    
    write_to_can2_buffer(goma, 0);
 */  
    //send_BPS_pressure_0_analog (&status);

    return;
}

unsigned int device_id (void){
    return DEVICE_ID_TE;
}


int main (void) 
{
    status.Debug_Mode = true;
    status.Raw_Mode = false;
    
    config_can2();
    send_init_msg();

//Binds the EE address to the EE data vector and loads the EE with the defined values    
    _init_prog_address (thresholds_EE_address, initial_EE_thresholds);

//Initializes threshold structure with defined values and EE values
    init_thresholds (thresholds_EE_address, &threshold, stored_EE_thresholds);
    
    init_adc();
    config_timer1(25, 4);

    TRISDbits.TRISD2 = 0;


    TRISBbits.TRISB12 = 0;
    LATBbits.LATB12 = 1;

    CANdata *received_message;

    while(1) 
    {    
        acquire_values  (&adc_flag, &value);
        proccess_values (&timer1_flag, &value, &status, &threshold, &timer_counter);

        send_can2_buffer();

        received_message = pop_can2();
        if(received_message != NULL)
            process_can_message (received_message, 
                                 &status,
                                 &value, 
                                 thresholds_EE_address,
                                 stored_EE_thresholds,
                                 &threshold);

        ClrWdt();
        Idle();
    }   

return 0;
}
