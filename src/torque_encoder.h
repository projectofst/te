#ifndef _TORQUE_ENCODER_H
#define _TORQUE_ENCODER_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/TE_CAN.h"


#include "lib_pic30f/EEPROM.h"

//#include "send_functions.h"
//#include "update_functions.h"


#define DEBUG 1 //0 debug off, 1 debug on

#define LOWER_SAFE_ZONE 50 
#define UPPER_SAFE_ZONE 50

#define ZERO_FORCE_DEADZONE 200
#define MAX_FORCE_DEADZONE  200

#define ZERO_FORCE_DEADZONE_BPS_ELECTRIC 50
#define MAX_FORCE_DEADZONE_BPS_ELECTRIC  50

#define APPS_0_GND_THRS 490
#define APPS_0_VCC_THRS 3280
#define APPS_0_LOWER_MECHANICAL_THRS 650
#define APPS_0_UPPER_MECHANICAL_THRS 3150

#define APPS_1_GND_THRS 950
#define APPS_1_VCC_THRS 3815
#define APPS_1_LOWER_MECHANICAL_THRS 1350
#define APPS_1_UPPER_MECHANICAL_THRS 3650

#define BPS_ELECTRIC_GND_THRS 300
#define BPS_ELECTRIC_VCC_THRS 3800
#define BPS_ELECTRIC_LOWER_MECHANICAL_THRS 750
#define BPS_ELECTRIC_UPPER_MECHANICAL_THRS 1150

#define BPS_PRESSURE_0_GND_THRS 370
#define BPS_PRESSURE_0_VCC_THRS 3800
#define BPS_PRESSURE_0_LOWER_MECHANICAL_THRS 400
#define BPS_PRESSURE_0_UPPER_MECHANICAL_THRS 1000

#define BPS_PRESSURE_1_GND_THRS 370
#define BPS_PRESSURE_1_VCC_THRS 3800
#define BPS_PRESSURE_1_LOWER_MECHANICAL_THRS 400
#define BPS_PRESSURE_1_UPPER_MECHANICAL_THRS 950

#define NUMBER_OF_AVERAGE_POINTS 16
#define NUMBER_OF_COUNTERS 30


typedef struct counters {
    unsigned int BPS_electric;
    unsigned int BPS_pressure;
    unsigned int APPS;

}COUNTERS;



typedef struct {

    unsigned int BPS_electric_0 [NUMBER_OF_AVERAGE_POINTS];
    unsigned int BPS_electric_1 [NUMBER_OF_AVERAGE_POINTS];
    unsigned int BPS_pressure_0 [NUMBER_OF_AVERAGE_POINTS];
    unsigned int BPS_pressure_1 [NUMBER_OF_AVERAGE_POINTS];
    unsigned int APPS_0 [NUMBER_OF_AVERAGE_POINTS];
    unsigned int APPS_1 [NUMBER_OF_AVERAGE_POINTS];
    
    unsigned int new_BPS_electric_0;
    unsigned int new_BPS_electric_1;
    unsigned int new_BPS_pressure_0;
    unsigned int new_BPS_pressure_1;
    unsigned int new_APPS_0;
    unsigned int new_APPS_1;

    unsigned int avg_BPS_electric_0;
    unsigned int avg_BPS_electric_1;
    unsigned int avg_BPS_pressure_0;
    unsigned int avg_BPS_pressure_1;
    unsigned int avg_APPS_0;
    unsigned int avg_APPS_1;

    uint16_t accelarator;
    uint16_t brake_electric;
    uint16_t brake_pressure;

    uint16_t raw_avg_APPS_0;
    uint16_t raw_avg_APPS_1;
    uint16_t raw_avg_BPS_electric;
    uint16_t raw_avg_BPS_pressure_0;
    uint16_t raw_avg_BPS_pressure_1;


    unsigned int index;

}TE_VALUES;

typedef struct {

    volatile bool new_thresholds;

    volatile uint16_t gnd;
    volatile uint16_t vcc;
    
    volatile uint16_t lower_mechanical;
    volatile uint16_t upper_mechanical;
 
    volatile uint16_t zero_force;
    volatile uint16_t max_force;

    volatile uint16_t range;

}SENSOR_THRESHOLDS;

typedef struct {

    SENSOR_THRESHOLDS APPS_0;
    SENSOR_THRESHOLDS APPS_1;
    SENSOR_THRESHOLDS BPS_pressure_0;
    SENSOR_THRESHOLDS BPS_pressure_1;
    SENSOR_THRESHOLDS BPS_electric;

}TE_THRESHOLDS;



typedef enum {

APPS_0_LOWER_MEC,
APPS_0_UPPER_MEC,
APPS_1_LOWER_MEC,
APPS_1_UPPER_MEC,
BPS_ELECTRIC_LOWER_MEC,
BPS_ELECTRIC_UPPER_MEC,
BPS_PRESSURE_0_LOWER_MEC,
BPS_PRESSURE_0_UPPER_MEC,
BPS_PRESSURE_1_LOWER_MEC,
BPS_PRESSURE_1_UPPER_MEC,

}THRESHOLD_SELECT;

#endif