#include "update_functions.h"

void update_selected_mechanical_thresholds (INTERFACE_MSG_PEDAL_THRESHOLDS select, 
                                            TE_VALUES *values,
                                            _prog_addressT EE_address, 
                                            uint16_t new_mechanical_thresholds[16],
                                            TE_THRESHOLDS *thresholds){

    switch (select){
        case BRAKE_LOWER:
            new_mechanical_thresholds[BPS_ELECTRIC_LOWER_MEC]   = values->raw_avg_BPS_electric;
            new_mechanical_thresholds[BPS_PRESSURE_0_LOWER_MEC] = values->raw_avg_BPS_pressure_0;
            new_mechanical_thresholds[BPS_PRESSURE_1_LOWER_MEC] = values->raw_avg_BPS_pressure_1;

            check_threshold_overlap(LOWER, &thresholds->BPS_electric,   &values->raw_avg_BPS_electric);
            check_threshold_overlap(LOWER, &thresholds->BPS_pressure_0, &values->raw_avg_BPS_pressure_0);
            check_threshold_overlap(LOWER, &thresholds->BPS_pressure_1, &values->raw_avg_BPS_pressure_1);
            break;

        case BRAKE_UPPER:
            new_mechanical_thresholds[BPS_ELECTRIC_UPPER_MEC]   = values->raw_avg_BPS_electric;
            new_mechanical_thresholds[BPS_PRESSURE_0_UPPER_MEC] = values->raw_avg_BPS_pressure_0;
            new_mechanical_thresholds[BPS_PRESSURE_1_UPPER_MEC] = values->raw_avg_BPS_pressure_1;

            check_threshold_overlap(UPPER, &thresholds->BPS_electric,   &values->raw_avg_BPS_electric);
            check_threshold_overlap(UPPER, &thresholds->BPS_pressure_0, &values->raw_avg_BPS_pressure_0);
            check_threshold_overlap(UPPER, &thresholds->BPS_pressure_1, &values->raw_avg_BPS_pressure_1);
            break;

        case ACCELERATOR_LOWER:

            new_mechanical_thresholds[APPS_0_LOWER_MEC] = values->raw_avg_APPS_0;
            new_mechanical_thresholds[APPS_1_LOWER_MEC] = values->raw_avg_APPS_1;

            check_threshold_overlap(LOWER, &thresholds->APPS_0, &values->raw_avg_APPS_0);
            check_threshold_overlap(LOWER, &thresholds->APPS_1, &values->raw_avg_APPS_1);
            break;

        case ACCELERATOR_UPPER:
        
            new_mechanical_thresholds[APPS_0_UPPER_MEC] = values->raw_avg_APPS_0;
            new_mechanical_thresholds[APPS_1_UPPER_MEC] = values->raw_avg_APPS_1;

            check_threshold_overlap(UPPER, &thresholds->APPS_0, &values->raw_avg_APPS_0);
            check_threshold_overlap(UPPER, &thresholds->APPS_1, &values->raw_avg_APPS_1);
            break;

        default:
            break;
    }

    eeprom_write_row (EE_address, new_mechanical_thresholds);

    return;
}

void check_threshold_overlap (SAFE_ZONE_SELECT safe_zone, SENSOR_THRESHOLDS *thresholds, uint16_t *raw_average){

    switch(safe_zone){
        case LOWER:
            if((*raw_average - LOWER_SAFE_ZONE) <= thresholds->gnd)
                thresholds->lower_mechanical = thresholds->gnd + 1;
            else
                thresholds->lower_mechanical = *raw_average - LOWER_SAFE_ZONE;

            thresholds->zero_force = *raw_average + ZERO_FORCE_DEADZONE;
            thresholds->range = thresholds->max_force - thresholds->zero_force;
            break;

        case UPPER:
            if((*raw_average + UPPER_SAFE_ZONE) >= thresholds->vcc)
                thresholds->upper_mechanical = thresholds->vcc - 1;
            else
                thresholds->upper_mechanical = *raw_average + UPPER_SAFE_ZONE;

            thresholds->max_force = *raw_average - MAX_FORCE_DEADZONE;
            thresholds->range = thresholds->max_force - thresholds->zero_force;
            break;

        default:
            break;
    }
    return; 
}


void init_mechanical_thresholds (SENSOR_THRESHOLDS *thresholds){

    if((thresholds->lower_mechanical - LOWER_SAFE_ZONE) <= thresholds->gnd)
        thresholds->lower_mechanical = thresholds->gnd + 1;
    else
        thresholds->lower_mechanical = thresholds->lower_mechanical - LOWER_SAFE_ZONE;
   
    if((thresholds->upper_mechanical + UPPER_SAFE_ZONE) >= thresholds->vcc)
        thresholds->upper_mechanical = thresholds->vcc - 1;
    else
        thresholds->upper_mechanical = thresholds->upper_mechanical + UPPER_SAFE_ZONE;         
    return; 
}

void update_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select, TE_THRESHOLDS *thresholds){

    switch(select){

        case BRAKE_LOWER:

            thresholds->BPS_electric.zero_force   = thresholds->BPS_electric.lower_mechanical   + ZERO_FORCE_DEADZONE_BPS_ELECTRIC;
            thresholds->BPS_pressure_0.zero_force = thresholds->BPS_pressure_0.lower_mechanical + ZERO_FORCE_DEADZONE;
            thresholds->BPS_pressure_1.zero_force = thresholds->BPS_pressure_1.lower_mechanical + ZERO_FORCE_DEADZONE;
   
            thresholds->BPS_pressure_0.range = thresholds->BPS_pressure_0.max_force - thresholds->BPS_pressure_0.zero_force;
            thresholds->BPS_pressure_1.range = thresholds->BPS_pressure_1.max_force - thresholds->BPS_pressure_1.zero_force;
            thresholds->BPS_electric.range   = thresholds->BPS_electric.max_force   - thresholds->BPS_electric.zero_force;
            return;
            break;

        case BRAKE_UPPER:

            thresholds->BPS_electric.max_force    = thresholds->BPS_electric.upper_mechanical   - MAX_FORCE_DEADZONE_BPS_ELECTRIC;
            thresholds->BPS_pressure_0.max_force  = thresholds->BPS_pressure_0.upper_mechanical - MAX_FORCE_DEADZONE;
            thresholds->BPS_pressure_1.max_force  = thresholds->BPS_pressure_1.upper_mechanical - MAX_FORCE_DEADZONE;

            thresholds->BPS_pressure_0.range = thresholds->BPS_pressure_0.max_force - thresholds->BPS_pressure_0.zero_force;
            thresholds->BPS_pressure_1.range = thresholds->BPS_pressure_1.max_force - thresholds->BPS_pressure_1.zero_force;
            thresholds->BPS_electric.range   = thresholds->BPS_electric.max_force   - thresholds->BPS_electric.zero_force;
            return;
            break;

        case ACCELERATOR_LOWER:

            thresholds->APPS_0.zero_force = thresholds->APPS_0.lower_mechanical + ZERO_FORCE_DEADZONE;
            thresholds->APPS_1.zero_force = thresholds->APPS_1.lower_mechanical + ZERO_FORCE_DEADZONE;
    
            thresholds->APPS_0.range = thresholds->APPS_0.max_force - thresholds->APPS_0.zero_force;
            thresholds->APPS_1.range = thresholds->APPS_1.max_force - thresholds->APPS_1.zero_force;
            return;
            break;

        case ACCELERATOR_UPPER:

            thresholds->APPS_0.max_force  = thresholds->APPS_0.upper_mechanical - MAX_FORCE_DEADZONE;
            thresholds->APPS_1.max_force  = thresholds->APPS_1.upper_mechanical - MAX_FORCE_DEADZONE;
    
            thresholds->APPS_0.range = thresholds->APPS_0.max_force - thresholds->APPS_0.zero_force;
            thresholds->APPS_1.range = thresholds->APPS_1.max_force - thresholds->APPS_1.zero_force;
            return;
            break;
        
        default:
            return;
            break;
    }
}
    

void update_ranges (TE_THRESHOLDS *thresholds)
{
    thresholds->APPS_0.range         = thresholds->APPS_0.max_force         - thresholds->APPS_0.zero_force;
    thresholds->APPS_1.range         = thresholds->APPS_1.max_force         - thresholds->APPS_1.zero_force;
    thresholds->BPS_pressure_0.range = thresholds->BPS_pressure_0.max_force - thresholds->BPS_pressure_0.zero_force;
    thresholds->BPS_pressure_1.range = thresholds->BPS_pressure_1.max_force - thresholds->BPS_pressure_1.zero_force;
    thresholds->BPS_electric.range   = thresholds->BPS_electric.max_force   - thresholds->BPS_electric.zero_force;

    return;
}
/*
*********************************************************************
|   Name:           update_values
|				
|	Description: 	Updates 5 arrays by adding one new value for each 
|                   and removing the oldest one 	
|
|   Priority:       - 
|
|	Arguments:	    Pointer to the structure containing the values
|	Returns:	    -
|
|   Tested:         Yes
*********************************************************************
*/
void update_values (TE_VALUES *value) 
{
    value->BPS_electric_0 [value->index] = value->new_BPS_electric_0;
    value->BPS_pressure_0 [value->index] = value->new_BPS_pressure_0;
    value->BPS_pressure_1 [value->index] = value->new_BPS_pressure_1;
    value->APPS_0 [value->index] = value->new_APPS_0;
    value->APPS_1 [value->index] = value->new_APPS_1;

    if (value->index >= (NUMBER_OF_AVERAGE_POINTS - 1))
        value->index = 0;
    else
        value->index++;
    
    return;
}


/*
*********************************************************************
|   Name:           update_raw_averages
|				
|	Description: 	Updates 5 averages using the values stored in the
|                   arrays corresponding to each variable	
|
|   Priority:       - 
|
|	Arguments:	    Pointer to the structure containing the values
|	Returns:	    -
|
|   Tested:         Yes
*********************************************************************
*/
void update_raw_averages (TE_VALUES *value){

    uint16_t i = 0;
    uint16_t sum[5] = {0};

    for (i = 0; i < NUMBER_OF_AVERAGE_POINTS; i++)
    {
        sum[0] += (value->BPS_electric_0[i]);
        sum[1] += (value->BPS_pressure_0[i]);
        sum[2] += (value->BPS_pressure_1[i]);
        sum[3] += (value->APPS_0[i]);
        sum[4] += (value->APPS_1[i]);
    }

    value->avg_BPS_electric_0 = (float) (sum[0] / NUMBER_OF_AVERAGE_POINTS);
    value->avg_BPS_pressure_0 = (float) (sum[1] / NUMBER_OF_AVERAGE_POINTS);
    value->avg_BPS_pressure_1 = (float) (sum[2] / NUMBER_OF_AVERAGE_POINTS);
    value->avg_APPS_0 = (float) (sum[3] / NUMBER_OF_AVERAGE_POINTS);
    value->avg_APPS_1 = (float) (sum[4] / NUMBER_OF_AVERAGE_POINTS);

    value->raw_avg_APPS_0         = value-> avg_APPS_0;
    value->raw_avg_APPS_1         = value-> avg_APPS_1;
    value->raw_avg_BPS_electric   = value-> avg_BPS_electric_0;
    value->raw_avg_BPS_pressure_0 = value-> avg_BPS_pressure_0;
    value->raw_avg_BPS_pressure_1 = value-> avg_BPS_pressure_1;

    return;
}


/*
*********************************************************************
|   Name:           update_treated_averages
|				
|	Description: 	Subtracts the lower bound from the raw average	
|                   and calculates the percentage for all 5 variables
|   Priority:       - 
|
|	Arguments:	    Pointer to the structure containing the values
|	Returns:	    -
|
|   Tested:         No
*********************************************************************
*/
void update_treated_averages (TE_VALUES *value, TE_THRESHOLDS *thresholds, TE_CORE *status){

    update_sensor_value (APPS_0,         &value->avg_APPS_0,         &thresholds->APPS_0,         status);
    update_sensor_value (APPS_1,         &value->avg_APPS_1,         &thresholds->APPS_1,         status);                 
    update_sensor_value (BPS_ELECTRIC,   &value->avg_BPS_electric_0, &thresholds->BPS_electric,   status);
    update_sensor_value (BPS_PRESSURE_0, &value->avg_BPS_pressure_0, &thresholds->BPS_pressure_0, status);
    update_sensor_value (BPS_PRESSURE_1, &value->avg_BPS_pressure_1, &thresholds->BPS_pressure_1, status);

    value->accelarator        = (float) ((value->avg_APPS_0 + value->avg_APPS_1) / 2);
    value->brake_pressure     = value->avg_BPS_pressure_0;
    value->brake_electric     = value->avg_BPS_electric_0;
    
    return;
}


void update_sensor_value (SENSOR_SELECT sensor,
                          uint16_t *course, 
                          SENSOR_THRESHOLDS *thresholds, 
                          TE_CORE *status){

    if(*course <= thresholds->gnd){
        *course = 0;
        update_sensor_status(sensor, SHORT_CIRCUIT, status);
    }
    else if(*course <= thresholds->lower_mechanical){
        *course = 0;
        update_sensor_status(sensor, OVERSHOOT, status);
    }
    else if(*course <= thresholds->zero_force){
        *course = 0;
        update_sensor_status(sensor, NORMAL, status);
    }
    else if(*course >= thresholds->vcc){
        *course = 0;
        update_sensor_status(sensor, SHORT_CIRCUIT, status);
    }
    else if(*course >= thresholds->upper_mechanical){
        *course = 10000;
        update_sensor_status(sensor, OVERSHOOT, status);
    }
    else if(*course >= thresholds->max_force){
        *course = 10000;
        update_sensor_status(sensor, NORMAL, status);
    }
    else{
        *course -= thresholds->zero_force;
        *course = (float) ((*course * 10000.0) / (thresholds->range));
        update_sensor_status(sensor, NORMAL, status);
    }

    return;
}


void update_sensor_status (SENSOR_SELECT sensor, POSITION_SELECT position ,TE_CORE *status){

    switch(sensor){
/*******************************************/        
        case APPS_0:
            switch(position){

                case SHORT_CIRCUIT:
                    status->CC_APPS_0 = true;
                    status->Overshoot_APPS_0 = true;
                    break;
                case OVERSHOOT:
                    status->CC_APPS_0 = false;
                    status->Overshoot_APPS_0 = true;
                    break;
                case NORMAL:
                    status->CC_APPS_0 = false;
                    status->Overshoot_APPS_0 = false;
                    break;
            break;
            }
/*******************************************/            
        case APPS_1:
            switch(position){

                case SHORT_CIRCUIT:
                    status->CC_APPS_1 = true;
                    status->Overshoot_APPS_1 = true;
                    break;
                case OVERSHOOT:
                    status->CC_APPS_1 = false;
                    status->Overshoot_APPS_1 = true;
                    break;
                case NORMAL:
                    status->CC_APPS_1 = false;
                    status->Overshoot_APPS_1 = false;
                    break;
            break;
            }
/*******************************************/
        case BPS_ELECTRIC:
            switch(position){

                case SHORT_CIRCUIT:
                    status->CC_BPS_electric_0 = true;
                    status->Overshoot_BPS_electric = true;
                    break;
                case OVERSHOOT:
                    status->CC_BPS_electric_0 = false;
                    status->Overshoot_BPS_electric = true;
                    break;
                case NORMAL:
                    status->CC_BPS_electric_0 = false;
                    status->Overshoot_BPS_electric = false;
                    break;
            break;
            }
/*******************************************/        
        case BPS_PRESSURE_0:
            switch(position){

                case SHORT_CIRCUIT:
                    status->CC_BPS_pressure_0 = true;
                    status->Overshoot_BPS_pressure_0 = true;
                    break;
                case OVERSHOOT:
                    status->CC_BPS_pressure_0 = false;
                    status->Overshoot_BPS_pressure_0 = true;
                    break;
                case NORMAL:
                    status->CC_BPS_pressure_0 = false;
                    status->Overshoot_BPS_pressure_0 = false;
                    break;
            break;
            }
/*******************************************/
        case BPS_PRESSURE_1:
            switch(position){

                case SHORT_CIRCUIT:
                    status->CC_BPS_pressure_1 = true;
                    status->Overshoot_BPS_pressure_1 = true;
                    break;
                case OVERSHOOT:
                    status->CC_BPS_pressure_1 = false;
                    status->Overshoot_BPS_pressure_1 = true;
                    break;
                case NORMAL:
                    status->CC_BPS_pressure_1 = false;
                    status->Overshoot_BPS_pressure_1 = false;
                    break;
            break;
            }
    }
    return;
}